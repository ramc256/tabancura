<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingBook extends Model
{
    protected $fillable = [

        'id', 'concept','date', 'amount', 'type', 'category_id'
    ];

    public function category() 
    { 
    	return $this->belongsTo('App\Category');
    	
    }
}
