<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    protected $fillable = [
        'name', 'run', 'address','phone','amount_day_delay',
    ];

    public function user()
    {
        
        return $this->belongsTo('App\User');
    }

}


