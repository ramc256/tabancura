<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\AccountingBook;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::all();
        return view('category.index')->with(['categories'=>$categories]);

    }

    public function create()
    {
         return view('category.create');
    }   

     public function store(Request $request)
    {
        Category::create($request->all());

         return redirect()->route('category.index');
    }   
      public function edit($id)
    {
        $category=Category::find($id);
         return view('category.edit')->with(['category'=>$category]);
    }   

    public function update(Request $request,$id)
    {
        $category = Category::find($id);

        $category->fill(request()->all());

        $category->save();
        
        return redirect()->route('category.index');
    }



    public function show()
    {
        return view('category.show') -> with(['id' => request() -> id]);
    }

    public function destroy($id)
    {
        AccountingBook::where('category_id', $id)->delete();
        
        $category = Category::destroy($id); 

        return redirect()->route('category.index');
    }
}
