<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', 'Tabancura')</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
 
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">
     <!-- Bootstrap core CSS     -->
     <link href="{{ asset('material-dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('material-dashboard/assets/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('material-dashboard/assets/css/demo.css" rel="stylesheet') }}" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="http://www.institutotabancurasa.cl" class="simple-text">
                   <img src="{{ asset('img/logo-tabancura.png') }}" style="height: 100px;">
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="active">
                        <a href="{{route('home')}}">
                            <i class="material-icons">home</i>
                            <p>Tabancura Mantenedor</p>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">dashboard</i>
                            <p>Menú Principal</p>
                            <p class="hidden-lg hidden-md">dashboard</p>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">Registro de Alumnos</a>
                            </li>
                            <li>
                                <a href="#">Inscribir un Nuevo Alumno</a>
                            </li>
                            <li>
                                <a href="#">Reportes</a>
                            </li>
                            <li>
                                <a href="#">Egresos e Ingresos de Sedes</a>
                            </li>
                            <li>
                                <a href="{{url('/category')}}">Catalogos</a>
                            </li>
                            <li>
                                <a href="{{url('/accountingbook')}}">Libro Contable</a>
                            </li>
                            <li>
                                <a href="#">Certificados Alumno Regular</a>
                            </li>
                            <li>
                                <a href="#">Depositos Administrativos</a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">https</i>
                                <p>Seguridad</p>
                                <p class="hidden-lg hidden-md">https</p>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">Usuarios del Sistema</a>
                            </li>
                            <li>
                                <a href="#">Auditoria</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
<div class="main-panel">
@include('layouts.navbar')


    @yield('content')
</div>
@include('layouts.footer')
    


<script src="{{ asset('material-dashboard/assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('material-dashboard/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('material-dashboard/assets/js/material.min.js') }}" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="{{ asset('material-dashboard/assets/js/chartist.min.js') }}"></script>
<!--  Dynamic Elements plugin -->
<script src="{{ asset('material-dashboard/assets/js/arrive.min.js') }}"></script>
<!--  PerfectScrollbar Library -->
<script src="{{ asset('material-dashboard/assets/js/perfect-scrollbar.jquery.min.js') }}"></script>
<!-- Material Dashboard javascript methods -->
<script src="{{ asset('material-dashboard/assets/js/material-dashboard.js?v=1.2.0') }}"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ asset('material-dashboard/assets/js/demo.js') }}"></script>
</body>
</html>