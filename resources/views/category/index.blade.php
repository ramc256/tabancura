
@extends('layouts.master')

@section('content')
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                          <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Catalogos</h4>
                                    <p class="category">Listado de Catalagos</p>
                                    
                                </div>
                                <div class="pull-left">
                                         <a href="{{route('category.create')}}" class="btn btn-primary" id="addButton">Nuevo</a>
                                    </div>
                                    <br>
                                    <br>
                                    <br>    
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th>Tipo</th>
                                            <th>Acciones</th>
                                        </thead>
                                        <tbody>
                                        @foreach($categories as $element)
                                        <tr>
                                            <td>{{$element -> id}}</td>
                                            <td>{{$element -> name}}</td>
                                            <td>{{$element -> description}}</td> 
                                            <td>{{$element -> type}}</td>   
                                            <td>
                                                <a href="{{route('category.edit',$element-> id)}}"><i class="material-icons text-info">mode_edit</i></a> 
                                            <form action="{{ route('category.destroy', $element->id) }}" method="POST"> 
                                        {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit"><i class="material-icons text-danger" >delete</i></button>
                                            </form></td>             
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div> 
        </div>
  

@endsection

