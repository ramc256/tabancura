
@extends('layouts.master')

@section('content')
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                          <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Crear - Catalago</h4>
                                    <p class="category">Creación de Catalago</p>
                                    
                                </div>
                                <div class="pull-left">
                                         <a href="{{route('category.index')}}" class="btn btn-warning" id="addButton">Atrás</a>
                                    </div>
                                    <br>
                                    <br>
                                    <br>   
                                    <form action="{{ route('category.store') }}" method="POST"  enctype="multipart/form-data">
                                       {{ csrf_field() }}
                                      <div class="form-group">
                                         <label for="exampleInput1" class="bmd-label-floating">Nombre</label>
                                         <input type="text" name="name" class="form-control" id="description">
                                         <span class="bmd-help">Descripción de catalago</span>
                                      </div>
                                      <div class="form-group">
                                         <label for="exampleInput1" class="bmd-label-floating">Descripción</label>
                                         <input type="text" name="description" class="form-control" id="description">
                                         <span class="bmd-help">Descripción de catalago</span>
                                      </div>
                                     <div class="form-group">
                                        <label for="type">tipo</label>
                                        <select class="form-control" name="type" id="type">
                                          <option value="ingreso">INGRESO</option>
                                          <option value="egreso">EGRESO</option>
                                        </select>
                                    </div>
                                      <button type="submit" class="btn btn-primary">Guardar</button>
                                    </form> 
                               
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div> 
        </div>
  

@endsection

