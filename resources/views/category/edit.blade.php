
@extends('layouts.master')

@section('content')
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                          <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Editar - Catalago</h4>
                                    <p class="category">Edición de Catalago</p>
                                    
                                </div>
                                <div class="pull-left">
                                         <a href="{{route('category.index')}}" class="btn btn-warning" id="addButton">Atrás</a>
                                    </div>
                                    <br>
                                    <br>
                                    <br>   
                                    <form action="{{ route('category.update', $category->id) }}" method="POST">
                                      {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT">
                                      <div class="form-group">
                                         <label name="description" for="exampleInput1" class="bmd-label-floating">Nombre</label>
                                         <input name="name" type="text" class="form-control" id="description" value="{{$category->name}}">
                                         <span class="bmd-help">Nombre de catalago</span>
                                      </div>
                                      <div class="form-group">
                                         <label name="description" for="exampleInput1" class="bmd-label-floating">Descrpción</label>
                                         <input name="description" type="text" class="form-control" id="description" value="{{$category->description}}">
                                         <span class="bmd-help">Descripción de catalago</span>
                                      </div>
                                     <div class="form-group">
                                        <label for="type">tipo</label>
                                        <select name="type" class="form-control" id="type">
                                          <option value="ingreso">Ingreso</option>
                                          <option value="egreso">Egreso</option>
                                        </select>
                                    </div>
                                      <button type="submit" class="btn btn-primary">Guardar</button>
                                    </form> 
                               
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div> 
        </div>
  

@endsection

