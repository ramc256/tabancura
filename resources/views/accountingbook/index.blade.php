
@extends('layouts.master')

@section('content')
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                          <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Libro Contable</h4>
                                    <p class="category">Listado de libro contable</p>
                                    
                                </div>
                                <div class="pull-left">
                                         <a href="{{route('accountingbook.create')}}" class="btn btn-primary" id="addButton">Nuevo</a>
                                    </div>
                                    <br>
                                    <br>
                                    <br>    
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <th>Codigo</th>
                                            <th>Concepto</th>
                                            <th>Tipo</th>
                                            <th>Monto</th>
                                            <th>Categoria</th>
                                            <th>Acciones</th>
                                        </thead>
                                        <tbody>
                                        @foreach($accountingbook as $element)
                                        <tr>
                                            <td>{{$element -> id}}</td>
                                            <td>{{$element -> concept}}</td> 
                                            <td>{{$element -> type}}</td>  
                                            <td>{{$element -> amount}}</td> 
                                            <td>{{$element ->category->name}}</td>
                                            <td><a href="{{route('accountingbook.edit',$element-> id)}}"><i class="material-icons text-info">mode_edit</i></a> 
                                            <a href="{{route('accountingbook.destroy', $element ->id )}}"><i class="material-icons text-danger" >delete</i></a></td>             
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div> 
        </div>
  

@endsection

