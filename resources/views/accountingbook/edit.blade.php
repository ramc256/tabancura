
@extends('layouts.master')

@section('content')
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                          <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Editar - Libro contable</h4>
                                    <p class="category">Edición del Libro contable</p>
                                    
                                </div>
                                <div class="pull-left">
                                         <a href="{{route('accountingbook.index')}}" class="btn btn-warning" id="addButton">Atrás</a>
                                    </div>
                                    <br>
                                    <br>
                                    <br>   
                                    <form action="{{ route('accountingbook.update', $accountingbook->id) }}" method="POST">
                                      {{ csrf_field() }}
                                      <div class="form-group">
                                         <label for="exampleInput1" class="bmd-label-floating">Concepto</label>
                                         <input name="concept" type="text" class="form-control" id="description">
                                         <span class="bmd-help">Concepto</span>
                                      </div>
                                     <div class="form-group">
                                        <label for="type">tipo</label>
                                        <select name="type" class="form-control" id="type">
                                          <option value="ingreso">Ingreso</option>
                                          <option value="egreso">Egreso</option>
                                        </select>
                                    </div>
                                      <button type="submit" class="btn btn-primary">Guardar</button>
                                    </form> 
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div> 
        </div>
  

@endsection


















