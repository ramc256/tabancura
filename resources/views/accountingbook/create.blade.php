
@extends('layouts.master')

@section('content')
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                          <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Crear - Libro contable</h4>
                                    <p class="category">Creación de libro contable</p>
                                    
                                </div>
                                <div class="pull-left">
                                         <a href="{{ route('accountingbook.index')}}" class="btn btn-warning" id="addButton">Atrás</a>
                                    </div>
                                    <br>
                                    <br>
                                    <br>   
                                    <form id="form-validation" action="{{ route('accountingbook.store') }}" method="POST"  enctype="multipart/form-data" >
                                      {{ csrf_field() }}
                                      <div class="form-group">
                                         <label for="exampleInput1" class="bmd-label-floating">Concepto</label>
                                         <input type="text" name="concept" class="form-control" id="concept">
                                         <span class="bmd-help">Concepto del libro contable</span>
                                      </div>
                                      <div class="form-group">
                                         <label for="exampleInput1" class="bmd-label-floating">Fecha</label>
                                         <input name="date" type="date" class="form-control" id="date">
                                         <span class="bmd-help">Fecha</span>
                                      </div>
                                     <div class="form-group">
                                        <label for="type">tipo</label>
                                        <select name="type" class="form-control" id="type">
                                          <option value="ingreso">INGRESO</option>
                                          <option value="egreso">EGRESO</option>
                                        </select>
                                        </div>
                                        <div class="form-group">
                                          <label for="catalago" class="control-label">Catalago</label>
                                            <select id="category_id" name="category_id" class="form-control">
                                              <option value="">Seleccione un catalago...</option>
                                                @foreach($category as $element)
                                                  <option value="{{ $element->id }}">{{ $element->name }}</option>
                                                    @endforeach
                                                </select>
                                          </div>
                                    <div class="form-group">
                                         <label for="exampleInput1" class="bmd-label-floating">Monto</label>
                                         <input name="amount" type="text" class="form-control" id="amount">
                                         <span class="bmd-help">Monto</span>
                                      </div>
                                      <button type="submit" class="btn btn-primary">Guardar</button>
                                    </form> 
                               
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div> 
        </div>
  

@endsection

