<?php

use Faker\Generator as Faker;

$factory->define(App\AccountingBook::class, function (Faker $faker) {
    return [
        'concept'=>$faker->word,
        'date'=>$faker->date,
       	'amount'=>$faker->randomNumber($nbDigits = 5),
       	'type'=>$faker->randomElement($array = array ('ingreso','egreso')),
       	'category_id'=>$faker->numberBetween($min = 1, $max = 30) 
    ];
});