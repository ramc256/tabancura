<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        
        'name'=>$faker->word,
       'description'=>$faker->word,
       'type'=>$faker->randomElement($array = array ('ingreso','egreso'))
    ];
});