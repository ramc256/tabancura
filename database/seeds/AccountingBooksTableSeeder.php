<?php

use Illuminate\Database\Seeder;
use App\AccountingBook;

class AccountingBooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AccountingBook::class,30)->create();
 
    }
}
