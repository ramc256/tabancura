<?php

use Illuminate\Database\Seeder;
use App\Institute;

class InstituteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
			[
				'name' => 'Santiago Centro', 
				'run' 	=> '78.121.700-k', 
				'address' 	=> 'Londres 98, Santiago Centro',
				'phone' 		=> '226 399 325',
				'amount_day_delay'   => '10',
				'created_at'=> new DateTime,
				'updated_at'=> new DateTime
			],

			[
				'name' => 'Providencia', 
				'run' 	=> '78.121.700-k', 
				'address' 	=> 'Av. Pedro de Valdivia Nº 1669, Providencia',
				'phone' 		=> '222 040 492 - 222 697 723',
				'amount_day_delay'   => '10',
				'created_at'=> new DateTime,
				'updated_at'=> new DateTime
			],

			[
				'name' => 'Vitacura', 
				'run' 	=> '78.121.700-k', 
				'address' 	=> 'Av. Kennedy Nº 8260, Vitacura',
				'phone' 		=> '222 294 380',
				'amount_day_delay'   => '10',
				'created_at'=> new DateTime,
				'updated_at'=> new DateTime
			],

			[
				'name' => 'Viña del Mar', 
				'run' 	=> '78.121.700-k', 
				'address' 	=> 'Traslaviña 371, Viña del Mar ',
				'phone' 		=> '322 621 718',
				'amount_day_delay'   => '10',
				'created_at'=> new DateTime,
				'updated_at'=> new DateTime
			],


		);

		Institute::insert($data);
    }
}
